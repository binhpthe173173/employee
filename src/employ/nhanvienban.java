/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employ;

import java.time.LocalDate;

/**
 *
 * @author admin
 */
public class nhanvienban implements Employee {

	private int id;
	private String fullName;
	private LocalDate birdDate;

	public nhanvienban(int id, String fullName, LocalDate birdDate) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.birdDate = birdDate;
	}

	String truong;
	String que;
	private int ordersSold;

	public nhanvienban(int id, String fullName, LocalDate birdDate, String truong, String que, int ordersSold) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.birdDate = birdDate;
		this.truong = truong;
		this.que = que;
		this.ordersSold = ordersSold;
	}

	public String getTruong() {
		return truong;
	}

	public void setTruong(String truong) {
		this.truong = truong;
	}

	public String getQue() {
		return que;
	}

	public void setQue(String que) {
		this.que = que;
	}

	public int getOrdersSold() {
		return ordersSold;
	}

	public void setOrdersSold(int ordersSold) {
		this.ordersSold = ordersSold;
	}
     @Override
    public void setID(int id) {
    this.id = id;
    }

    @Override
    public String getfullname() {
        return fullName ;}

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public LocalDate getBirdate() {
        return birdDate;
    }

    @Override
    public void setBirdate(LocalDate birdDate) {
        this.birdDate = birdDate;
    }

    @Override
    public String getBNF() {
        return "chi tra tien awn trua va dien thoai";
    }

    @Override
    public int getId() {
        return id;
    }
    
}
