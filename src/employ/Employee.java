/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employ;

import java.time.LocalDate;

/**
 *
 * @author admin
 */
public interface Employee {
    public String getBNF();
    int getId();
    public void setID(int id);
    String getfullname();
     public void setFullName(String fullName);
     LocalDate getBirdate();
     public void setBirdate(LocalDate birdDate);
}