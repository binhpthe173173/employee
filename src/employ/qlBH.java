/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employ;

import java.time.LocalDate;

/**
 *
 * @author admin
 */
public class qlBH implements Employee{
    private int id;
    private String fullName;
    private LocalDate birdDate;

    String truong;
    String que;
    String chungchi;
    private boolean kpiAchieved;

    public qlBH(int id, String fullName, LocalDate birdDate, String truong, String que, String chungchi, boolean kpiAchieved) {
        this.id = id;
        this.fullName = fullName;
        this.birdDate = birdDate;
        this.truong = truong;
        this.que = que;
        this.chungchi = chungchi;
        this.kpiAchieved = kpiAchieved;
    }
    @Override
    public void setID(int id) {
    this.id = id;
    }

    @Override
    public String getfullname() {
        return fullName ;}

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public LocalDate getBirdate() {
        return birdDate;
    }

    @Override
    public void setBirdate(LocalDate birdDate) {
        this.birdDate = birdDate;
    }

    @Override
    public String getBNF() {
        return "chi tra cho phi di lai";
    }

    @Override
    public int getId() {
        return id;
    }
    
}
